/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deber4.ocjp;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author FERNANDO
 */
public class Deber4OCJP {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)  {
        // TODO code application logic here  
        
        //Instanciando Clases
        Apple apple = new Apple();
        
        Fruit fruit = new Fruit();
        
        Citrus citrus = new Citrus();
        
        Orange orange = new Orange();
        
        Squeezable squeezable = new Squeezable() {};
        
        Object object = new Object();
        
        //Excepciones
        try {
            apple = (Apple) squeezable;
        } catch (Exception e) {
            System.err.println("Error Casting: Squezable to Apple");
        }
        
        try {
            citrus = (Citrus) fruit;
        } catch (Exception e) {
            System.err.println("Error Casting: Fruit to Citrus");
        }
        
        try {
            orange = (Orange) squeezable;
        } catch (Exception e) {
            System.err.println("Error Casting: Squeezable to Orange");
        }
        
        try {
            orange = (Orange) citrus;
        } catch (Exception e) {
            System.err.println("Error Casting: Citrus to Orange");
        }
        
        try {
            object = (Fruit)fruit;
        } catch (Exception e) {
            System.err.println("Error Casting: Fruit to Object");
        }
    }
}
